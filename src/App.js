import React, { Component } from "react";
import injectSheet from "react-jss";
import { Route, Link, Switch, Redirect } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { toggleTitleColor } from "./store";

const styles = theme => ({
  "@global body": {
    background: theme.palette.background,
    color: theme.palette.text
  },
  App: {
    padding: "20px",
    background: "black",
    maxWidth: "800px",
    minHeight: "100vh",
    height: "100%",
    margin: "auto",
    "&  h1": {
      fontSize: "5rem",
      textAlign: "center",
      cursor: "pointer"
    },
    "& input": {
      margin: "10px"
    },
    "& a": {
      color: theme.palette.text
    }
  },
  "title-primary": {
    color: theme.palette.primary
  },
  "title-secondary": {
    color: theme.palette.secondary
  }
});

const stp = state => ({
  titleColor: state.memeState.titleColor
});

const dtp = dispatch =>
  bindActionCreators(
    {
      toggleTitleColor: () => toggleTitleColor()
    },
    dispatch
  );
class App extends Component {
  state = {
    memeText: localStorage.getItem("memeText")
      ? localStorage.getItem("memeText")
      : "",
    videoPaused: false
  };

  componentDidMount() {
    const paused = localStorage.getItem("videoPaused") === "true";
    const text = localStorage.getItem("memeText")
      ? localStorage.getItem("memeText")
      : " ";
    const time = localStorage.getItem("currentTime")
      ? localStorage.getItem("currentTime")
      : 0;

    this.setState({ memeText: text, videoPaused: paused });

    if (paused) {
      this.refs.areaRef.value = text;
      this.refs.videoRef.currentTime = time;
      this.getVideoStill();
    } else {
      this.refs.areaRef.value = text;
      this.refs.videoRef.currentTime = time;
      this.refs.videoRef.play();
    }
  }

  pausePlayVideoHandler = () => {
    let paused = this.state.videoPaused;
    if (paused) {
      this.refs.videoRef.play();
      this.setState({ videoPaused: false });
      this.refs.canvas.style.visibility = "hidden";
    } else {
      this.refs.videoRef.pause();
      this.setState({ videoPaused: true });
      this.getVideoStill();
    }
  };

  playbackVideoHandler = () => {
    let paused = this.state.videoPaused;
    //Playback video and let user get the previous frame of the video
    if (paused) {
      this.refs.videoRef.currentTime = this.refs.videoRef.currentTime - 1 / 15;
      this.getVideoStill();
    }
  };

  forwardVideoHandler = () => {
    let paused = this.state.videoPaused;
    // Forward the video so user can get the next frame of the video
    if (paused) {
      this.refs.videoRef.currentTime = this.refs.videoRef.currentTime + 1 / 15;
      this.getVideoStill();
    }
  };

  getVideoStill() {
    const canvas = this.refs.canvas;

    // determine canvas height and width
    const canvasHeigth = this.refs.videoRef.height;
    this.refs.canvas.height = canvasHeigth;
    const ratio = 484 / 360;
    const canvasWidth = canvasHeigth * ratio;
    this.refs.canvas.width = canvasWidth;

    //set to visible
    this.refs.canvas.style.visibility = "visible";

    //draw canvas
    const ctx = canvas.getContext("2d");
    ctx.drawImage(this.refs.videoRef, 0, 0, canvasWidth, canvasHeigth);

    // Add text to canvas over multiple lines, split on new line
    let text = this.state.memeText;
    if (text != "") {
      let lines = text.split(/\n/);
      ctx.fillStyle = "white";
      ctx.font = "20pt Verdana";
      for (var i = 0; i < lines.length; i++) {
        ctx.fillText(lines[i], 10, (i + 1) * 40, canvasWidth - 20);
      }
    }
  }

  changeTextHandler = event => {
    let paused = this.state.videoPaused;
    if (paused) {
      this.setState({ memeText: event.target.value }, this.getVideoStill);
    } else {
      this.setState({ memeText: event.target.value });
    }
  };

  downloadImageHandler = () => {
    let paused = this.state.videoPaused;
    if (paused) {
      this.getVideoStill();
      const link = document.createElement("a");
      link.href = this.refs.canvas.toDataURL("image/jpg");
      link.download = "meme.jpg";
      document.body.appendChild(link);
      link.click();
    }
  };

  saveStateHandler = () => {
    localStorage.setItem("memeText", this.state.memeText);
    localStorage.setItem("videoPaused", this.state.videoPaused);
    localStorage.setItem("currentTime", this.refs.videoRef.currentTime);
  };

  render() {
    const { classes, titleColor, toggleTitleColor } = this.props;
    return (
      <div className={classes.App}>
        <header className="App-header">
          <h1
            onClick={toggleTitleColor}
            className={classes[`title-${titleColor}`]}
          >
            Vintage Meme Machine
          </h1>
        </header>
        <main>
          <Switch>
            <Route path="/home">
              <>
                <canvas
                  ref="canvas"
                  width="0"
                  height="0"
                  crossOrigin="Anonymous"
                />
                <video
                  width="800"
                  height="450"
                  playsInline
                  muted
                  loop
                  ref="videoRef"
                  crossOrigin="Anonymous"
                >
                  <source
                    src="https://upload.wikimedia.org/wikipedia/en/transcoded/6/61/Old_Man_Drinking_a_Glass_of_Beer_%281897%29.webm/Old_Man_Drinking_a_Glass_of_Beer_%281897%29.webm.360p.webm"
                    type="video/webm"
                  />
                </video>
                <div className="buttonGroup">
                  <button onClick={this.pausePlayVideoHandler.bind(this)}>
                    Play | Pause
                  </button>
                  <button onClick={this.playbackVideoHandler.bind(this)}>
                    Back
                  </button>
                  <button onClick={this.forwardVideoHandler.bind(this)}>
                    Forward
                  </button>
                  <button onClick={this.downloadImageHandler.bind(this)}>
                    Download
                  </button>
                  <button onClick={this.saveStateHandler.bind(this)}>
                    Save
                  </button>
                </div>
                <div className="addText">
                  <textarea
                    type="text"
                    placeholder="Something edgy..."
                    onChange={this.changeTextHandler}
                    ref="areaRef"
                  />
                </div>
              </>
            </Route>
            <Route path="/readme">
              <section>
                <h2>Devtest Readme</h2>
                <p>
                  Hello candidate, Welcome to our little dev test. The goal of
                  this exercise, is to asses your general skill level, and give
                  us something to talk about at our next appointment.
                </p>
                <section>
                  <h3>What this app should do</h3>
                  <p>
                    We'd like for you to build a tiny app called the "Vintage
                    Meme Machine". It will be a tool that allows users to
                    overlay text on a video, and capture or share it.
                  </p>
                  <p>These are the basic requirements:</p>
                  <ul>
                    <li>User can pick a point in the video</li>
                    <li>
                      User can enter text that is placed over the video{" "}
                      <em>still</em>
                    </li>
                    <li>
                      User can save this personalized content for later use
                    </li>
                  </ul>
                </section>
                <section>
                  <h3>What we want you to do</h3>
                  <p>
                    Off course we don't expect you to build a full fledged app
                    in such a short time frame.
                  </p>
                  <p>
                    But we would like for you to get in the basic requirements,
                    in one form or another. Beyond that feel free to show off
                    your strenghts as a frontend developer.
                  </p>
                  <p>Some ideas:</p>
                  <ul>
                    <li>Make it look really nice</li>
                    <li>Allow users to provide a custom video</li>
                    <li>Download the meme as an image file</li>
                    <li>Add fancy text effects to the text overlay</li>
                    <li>Push the resulting meme to a social media API</li>
                  </ul>
                </section>
                <section>
                  <p>
                    P.s. We've already added some libraries to make your life
                    easier (Redux, Jss, React Router), but feel free to add
                    more.
                  </p>
                </section>
              </section>
            </Route>
            <Redirect to="/home" />
          </Switch>
        </main>
        <footer>
          <nav>
            <ul>
              <li>
                <Link to="/home">Home</Link>
              </li>
              <li>
                <Link to="/readme">Readme</Link>
              </li>
            </ul>
          </nav>
        </footer>
      </div>
    );
  }
}

export default connect(
  stp,
  dtp
)(injectSheet(styles)(App));
